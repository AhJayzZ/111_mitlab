# 111_Mitlab 實驗室資訊

## Notes Branch

### [鐘德偉 TuckWai](https://gitlab.com/AhJayzZ/111_mitlab/-/tree/TuckWai)

### [王勁杰 JackWang](https://gitlab.com/AhJayzZ/111_mitlab/-/tree/JackWang)

### [林紹元 HankLin](https://gitlab.com/AhJayzZ/111_mitlab/-/tree/HankLin)

### [謝維 WadeHsieh](https://gitlab.com/AhJayzZ/111_mitlab/-/tree/WadeHsieh)

### [陳宣輔 BerryChen](https://gitlab.com/AhJayzZ/111_mitlab/-/tree/BarryChen)

---

## 過往研究
* 室內定位
* 深度學習應用於病毒靜態分析
* 深度學習應用於雷達手勢辨識
* 深度學習應用於雷達跌倒偵測
* 5G-ORAN平台建置
* 基於乙太鏈-區塊鏈智能合約
* 無人機自動避障
* 自然語言處理應用於社群媒體
* 深度學習應用於WIFI訊號與影像結合之室內定位
* 5G-Core
* 車速辨識
* 模型壓縮
* K8s分散式系統

## 畢業門檻
* 完成校外創意比賽
* 完成校內計畫案
* 完成國際期刊一篇

## 畢業出路
* 群聯
* 台積電
* 華碩
* 鴻海
* 漢翔


## 人格特質
* 自主性與主動性必須極高
* 積極參與實驗室事務(Ex.網管、報帳、帶學弟妹)
* 主動詢問學長姊有無事務需要幫忙
* 具有表達論述的能力
* 具有團隊合作的能力
* 具有會議記錄的能力
* 具有時間管理的能力
* 具有解決問題的能力

## 專業技能

### 基本學科
* 線性代數
* 機率與統計
* 資料結構與演算法
* 計算機組織
* 作業系統
* 網際網路概論
* 嵌入式系統設計
* 深度學習
* 機器學習

### 程式語言
* C
* C++
* Python
* PHP
* Java
* JavaScript
* Go

### 作業系統
* Windows
* Ubuntu
* CentOS
* Debian
* RTOS
### 應用程式
* Web Framework
* Database
* PyGUI
* VMware
* PaaS、SaaS
* Web Crawler
* DL Framework
* Mobile

### CI/CD
* GitLab/GitHub
* Jenkins
* Docker
* K8s
* ELK

## 新人預先準備

1. 題目發想
2. 建置環境
    - VMware
    - PaaS、SaaS
3. 應用層面
    - Web Framework
    - Database
    - Web Crawler
    - Mobile
    - DL Framework
4. CI/CD
    - GitLab/GitHub
    - Docker
    - Jenkins
    - K8s
    - ELK
